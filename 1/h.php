<?php

//ex-1
$var = 10;
echo $var;

echo "<br>";
echo "<br>";

if ($var == 10) {
    echo "No Changes !";
} else {
    echo "Unset has been done !";
};


echo "<br>";
echo "<br>";

unset($var);
$var = 5;
if ($var == 10) {
    echo "No Changes !";
} else {
    echo "Unset has been done !";
};


echo "<br>";
echo "<br>";
echo "<br>";
echo "<br>";

//ex-2

$var = 'Bangladesh';
echo $var;

echo "<br>";
echo "<br>";

if ($var == 'Bangladesh') {
    echo "No Changes found !";
} else {
    echo "Unset has been done !";
};


echo "<br>";
echo "<br>";

unset($var);
$var = 5;
if ($var == 5) {
    echo "No Changes !";
} else {
    echo "Unset has been done !";
};
