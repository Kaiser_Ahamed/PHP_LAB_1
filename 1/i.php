<?php
//is_bool ex-1

$var=False;
if(is_bool($var)){
    echo "Result is False";
}
else{
    echo "Result is True";
}

echo "<br>";
echo "<br>";

//is_bool ex-2

$var='10';
if(is_bool($var)){
    echo "Result is False";
}
else{
    echo "Result is True";
}
